from django.shortcuts import render, redirect

from main.forms import PersonForm
from main.models import Person


def main(request):
    people = Person.objects.all()
    return render(request, 'main.html', {'people': people})


def new(request):
    form = PersonForm()
    if request.POST:
        form = PersonForm(request.POST)
        if form.is_valid():
            person = form.save(commit=False)
            person.save()
            return redirect('/main')
    return render(request, 'new.html', {'form': form})
