from django import forms

from main.models import Person


class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = ('name', 'age')
